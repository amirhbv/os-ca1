#include "methods.h"

#define maxClientsNum 30
#define BUFFER_LEN 1024
#define MAX_FILE_NAME_LEN 1000

int heartbeatPort, listenPort = 22222;
struct sockaddr_in broadcastAddr, listenAddr;
int heartbeatSocket, listenSocket;

int clientSDs[maxClientsNum];
int maxSD;
fd_set readfds;
char buffer[BUFFER_LEN + 1], command[20], fileName[MAX_FILE_NAME_LEN];

void heartbeat(int signum)
{
    int ret = broadcast(
        heartbeatSocket,
        &broadcastAddr,
        "22222\0"
    );
    if (ret < 0)
    {
        perror("broadcast");
        return;
    }
    alarm(1);
}

void setupHeartbeatSocket()
{
    heartbeatSocket = socket(AF_INET, SOCK_DGRAM, 0);
    if (heartbeatSocket < 0)
    {
        perror("socket creation");
        exit(EXIT_FAILURE);
    }

    // set socket options enable broadcast
    int broadcastEnable = 1;
    int ret = setsockopt(
        heartbeatSocket,
        SOL_SOCKET,
        SO_BROADCAST,
        &broadcastEnable, sizeof(broadcastEnable)
    );
    if (ret < 0)
    {
        perror("heartbeat setsockopt");
        close(heartbeatSocket);
        exit(EXIT_FAILURE);
    }

    // Configure the port and ip we want to send to
    memset(&broadcastAddr, 0, sizeof(broadcastAddr));
    broadcastAddr.sin_family = AF_INET;
    broadcastAddr.sin_port = htons(heartbeatPort);
    broadcastAddr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
}

void setupTCPSocket()
{
    memset(&listenAddr, 0, sizeof(listenAddr));
    listenAddr.sin_family = AF_INET;
    listenAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    listenAddr.sin_port = htons(listenPort);

    listenSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (listenSocket < 0)
    {
        perror("socket createion");
        exit(EXIT_FAILURE);
    }

    int opt = 1;
    int ret = setsockopt(
        listenSocket,
        SOL_SOCKET,
        SO_REUSEADDR,
        &opt, sizeof(opt)
    );
    if (ret < 0)
    {
        perror("listen setsockopt");
        close(listenSocket);
        exit(EXIT_FAILURE);
    }

    ret = bind(
        listenSocket,
        (struct sockaddr *)&listenAddr, sizeof(listenAddr)
    );
    if (ret < 0)
    {
        perror("bind listen socket");
        close(listenSocket);
        exit(EXIT_FAILURE);
    }

    ret = listen(
        listenSocket,
        maxClientsNum
    );
    if (ret < 0)
    {
        perror("listen");
        close(listenSocket);
        exit(EXIT_FAILURE);
    }

    // TODO: log success

    for (int i = 0; i < maxClientsNum; i++)
    {
        clientSDs[i] = 0;
    }
}

int main(int argc, char const *argv[])
{
    if (argc != 2)
    {
        // TODO: log arguments len error
        exit(EXIT_FAILURE);
    }

    heartbeatPort = atoi(argv[1]);

    setupTCPSocket();
    setupHeartbeatSocket();

    signal(SIGALRM, heartbeat);
    heartbeat(SIGALRM);

    while (1)
    {
        //clear the socket set
        FD_ZERO(&readfds);

        //add master socket to set
        FD_SET(listenSocket, &readfds);
        maxSD = listenSocket;

        //add child sockets to set
        for (int i = 0; i < maxClientsNum; i++)
        {
            //socket descriptor
            int sockfd = clientSDs[i];

            //if valid socket descriptor then add to read list
            if (sockfd > 0)
                FD_SET(sockfd, &readfds);

            //highest file descriptor number, need it for the select function
            if (sockfd > maxSD)
                maxSD = sockfd;
        }

        //wait for an activity on one of the sockets , timeout is NULL ,
        //so wait indefinitely
        int activity = select(maxSD + 1, &readfds, NULL, NULL, NULL);

        if (activity < 0)
        {
            perror("select");
            // continue;
        }

        //If something happened on the master socket ,
        //then its an incoming connection
        int listenAddrLen = sizeof(listenAddr);
        int sockfd;
        if (FD_ISSET(listenSocket, &readfds))
        {
            int sockfd = accept(
                listenSocket,
                (struct sockaddr *)&listenAddr, (socklen_t *)&listenAddrLen
            );
            if (sockfd < 0)
            {
                perror("accept");
                continue;
            }

            //add new socket to array of sockets
            for (int i = 0; i < maxClientsNum; i++)
            {
                //if position is empty
                if (clientSDs[i] == 0)
                {
                    clientSDs[i] = sockfd;
                    break;
                }
            }
            print("Accept: ");
            printInt(sockfd);
        }

        //else its some IO operation on some other socket
        for (int i = 0; i < maxClientsNum; i++)
        {
            int sockfd = clientSDs[i];

            if (FD_ISSET(sockfd, &readfds))
            {
                print("RECV: ");
                printInt(sockfd);
                int ret = read(sockfd, buffer, BUFFER_LEN);
                print(buffer);
                //Check if it was for closing , and also read the
                //incoming message
                if (ret == 0)
                {
                    //Somebody disconnected
                    //Close the socket and mark as 0 in list for reuse
                    close(sockfd);
                    clientSDs[i] = 0;
                }

                //Echo back the message that came in
                else
                {
                    //set the string terminating NULL byte on the end
                    //of the data read
                    buffer[ret] = '\0';
                    print(buffer);
                    char **result = split(buffer, ' ');
                    strcpy(command, result[0]);
                    if (strcmp(command, "upload") == 0)
                    {
                        print("uploaaaaaaaaaaaad\n");
                    }
                    else if (strcmp(command, "download") == 0)
                    {
                        char fileContent[BUFFER_LEN + 1];
                        int ufd = open(result[1], O_RDONLY);
                        if (ufd <= 0)
                        {
                            perror("open file in server");
                        }

                        while (1)
                        {
                            memset(fileContent, '\0', BUFFER_LEN);
                            printInt(read(ufd, fileContent, BUFFER_LEN - 1));
                            int fileFinish = 0;
                            if (fileContent[0] == '\0')
                            {
                                memset(fileContent, '\0', BUFFER_LEN);
                                strcpy(fileContent, "FILEFINISH");
                                fileFinish = 1;
                            }
                            if (send(sockfd, fileContent, strlen(fileContent), 0) == -1)
                            {
                                perror("send");
                            }
                            // sleep(1);
                            if (fileFinish == 1)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        print("Command Not Found!\n");
                    }
                }
            }
        }
    }

    return 0;
}

