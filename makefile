CFLAGS = -Wall -pedantic -Iinclude
CC = gcc ${CFLAGS}

COMMON_OBJS = $(patsubst common/%.c, obj/%.o, $(wildcard common/*.c))

.PHONY: all clean build

build: obj server.out client.out

all: clean build

obj/methods.o: common/methods.c
	$(CC) -c common/methods.c -o obj/methods.o

obj/utils.o: common/utils.c
	$(CC) -c common/utils.c -o obj/utils.o

obj/server.o: server/main.c
	$(CC) -c server/main.c -o obj/server.o

obj/client.o: client/main.c
	$(CC) -c client/main.c -o obj/client.o

server.out: ${COMMON_OBJS} obj/server.o
	$(CC) ${COMMON_OBJS} obj/server.o -o server/server.out

client.out: ${COMMON_OBJS} obj/client.o
	$(CC) ${COMMON_OBJS} obj/client.o -o client/client.out

obj:
	mkdir -p obj

clean:
	rm *.o *.out
