#include "methods.h"

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET)
    {
        return &(((struct sockaddr_in *)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}

int broadcast(int sockfd, struct sockaddr_in *broadcastAddr, const char *message)
{
    print("broadcasting ");
    print(message);
    print("\n");
    return sendto(
        sockfd,
        message, strlen(message),
        0,
        (struct sockaddr *)broadcastAddr, sizeof(*broadcastAddr));
}

int receiveUDP(int sockfd, struct sockaddr_in *receiveAddr, char *recievdMessage, int maxReceiveLen)
{
    unsigned int receiveAddrSize = sizeof(*receiveAddr);

    return recvfrom(
        sockfd,
        recievdMessage, maxReceiveLen,
        MSG_DONTWAIT,
        (struct sockaddr *)receiveAddr, &receiveAddrSize
    );
}
