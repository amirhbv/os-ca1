#include "methods.h"

#define maxClientsNum 30
#define BUFFER_LEN 1024
#define MAX_FILE_NAME_LEN 1000

int heartbeatPort, listenPort;
struct sockaddr_in receiveAddr, serverAddr;
int heartbeatSocket, serverSocket;
char listenPortStr[5];
int connected;

int clientSDs[maxClientsNum];
int maxSD;
fd_set readfds;
char buffer[BUFFER_LEN + 1], command[20], fileName[MAX_FILE_NAME_LEN];

void handleHeartbeat(int signum)
{
    int ret = receiveUDP(
        heartbeatSocket,
        &receiveAddr,
        listenPortStr, 5);
    if (ret <= 0)
    {
        perror("receive");
        listenPort = 0;
    }
    else
    {
        if (!listenPort)
        {
            print("got server port\n");
        }
        listenPort = atoi(listenPortStr);
    }
    serverAddr.sin_port = htons(listenPort);
    // printInt(listenPort);
    alarm(1);
}

void setupServerSocket()
{
    memset(&serverAddr, 0, sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);

    serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (serverSocket < 0)
    {
        perror("socket createion");
        exit(EXIT_FAILURE);
    }

    print("server socket created\n");
}

void setupHeartbeatSocket()
{
    // Configure the port and ip we want to send to
    memset(&receiveAddr, 0, sizeof(receiveAddr));
    receiveAddr.sin_family = AF_INET;
    receiveAddr.sin_port = htons(heartbeatPort);
    receiveAddr.sin_addr.s_addr = htonl(INADDR_BROADCAST);

    heartbeatSocket = socket(AF_INET, SOCK_DGRAM, 0);
    if (heartbeatSocket < 0)
    {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    // set socket options enable broadcast
    int reusePerm = 1;
    int ret = setsockopt(
        heartbeatSocket,
        SOL_SOCKET,
        SO_REUSEADDR,
        &reusePerm, sizeof(reusePerm)
    );
    if (ret < 0)
    {
        perror("setsockopt 1");
        close(heartbeatSocket);
        exit(EXIT_FAILURE);
    }
    ret = setsockopt(
        heartbeatSocket,
        SOL_SOCKET,
        SO_REUSEPORT,
        &reusePerm, sizeof(reusePerm));
    if (ret < 0)
    {
        perror("setsockopt 2");
        close(heartbeatSocket);
        exit(EXIT_FAILURE);
    }

    ret = bind(
        heartbeatSocket,
        (struct sockaddr *)&receiveAddr, sizeof(receiveAddr));
    if (ret < 0)
    {
        print("bind heartbeat socket");
        close(heartbeatSocket);
        exit(EXIT_FAILURE);
    }

    print("heartbeat socket created\n");
}

void sendToServer()
{
    print("trying to connect\n");
    int ret;

    if (!connected)
    {
        ret = connect(
            serverSocket,
            (struct sockaddr *)&serverAddr, sizeof(serverAddr));
        if (ret < 0)
        {
            perror("connect");
            return;
        }
        connected = 1;
    }

    print("connected\n");

    ret = send(serverSocket, buffer, strlen(buffer), 0);
    printInt(ret);
    print(buffer);
    print("\n");

    char **result = split(buffer, ' ');
    strcpy(command, result[0]);
    if (strcmp(command, "upload") == 0)
    {
        char fileContent[BUFFER_LEN + 1];
        print(result[1]);
        int ufd = open(result[1], O_RDONLY, S_IRWXU);
        if (ufd < 0)
        {
            perror("open file in client");
        }
        while (1)
        {
            memset(fileContent, '\0', BUFFER_LEN);
            printInt(read(ufd, fileContent, BUFFER_LEN - 1));
            print(fileContent);
            int fileFinish = 0;
            if (fileContent[0] == '\0')
            {
                memset(fileContent, '\0', BUFFER_LEN);
                strcpy(fileContent, "FILEFINISH");
                fileFinish = 1;
            }
            if (send(serverSocket, fileContent, strlen(fileContent), 0) == -1)
            {
                perror("send");
            }
            // sleep(1);
            if (fileFinish == 1)
            {
                break;
            }
        }
    }
    else if (strcmp(command, "download") == 0)
    {
        print("downloaaaaaaaaaaad\n");
    }
    else
    {
        print("Command Not Found!\n");
    }

}

int main(int argc, char const *argv[])
{
    if (argc != 4)
    {
        // TODO: log arguments len error
        exit(EXIT_FAILURE);
    }

    heartbeatPort = atoi(argv[1]);

    setupHeartbeatSocket();
    setupServerSocket();

    signal(SIGALRM, handleHeartbeat);
    handleHeartbeat(SIGALRM);

    while (1)
    {
        //clear the socket set
        FD_ZERO(&readfds);

        //add master socket to set
        FD_SET(STDIN_FILENO, &readfds);
        maxSD = STDIN_FILENO;
        // maxSD = serverSocket;

        //add child sockets to set
        for (int i = 0; i < maxClientsNum; i++)
        {
            //socket descriptor
            int sockfd = clientSDs[i];

            //if valid socket descriptor then add to read list
            if (sockfd > 0)
                FD_SET(sockfd, &readfds);

            //highest file descriptor number, need it for the select function
            if (sockfd > maxSD)
                maxSD = sockfd;
        }

        //wait for an activity on one of the sockets , timeout is NULL ,
        //so wait indefinitely
        int activity = select(maxSD + 1, &readfds, NULL, NULL, NULL);

        if (activity < 0)
        {
            perror("select");
            // continue;
        }

        //If something happened on the master socket ,
        //then its an incoming connection
        if (FD_ISSET(STDIN_FILENO, &readfds))
        {
            read(STDIN_FILENO, buffer, BUFFER_LEN);
            if (listenPort)
            {
                sendToServer(buffer);
            }
            else
            {
                // p2p
            }
        }

        //else its some IO operation on some other socket
        // for (int i = 0; i < maxClientsNum; i++)
        // {
        //     int sockfd = clientSDs[i];

        //     if (FD_ISSET(sockfd, &readfds))
        //     {
        //         int ret = read(sockfd, buffer, BUFFER_LEN);
        //         //Check if it was for closing , and also read the
        //         //incoming message
        //         if (ret == 0)
        //         {
        //             //Somebody disconnected
        //             //Close the socket and mark as 0 in list for reuse
        //             close(sockfd);
        //             clientSDs[i] = 0;
        //         }

        //         //Echo back the message that came in
        //         else
        //         {
        //             //set the string terminating NULL byte on the end
        //             //of the data read
        //             buffer[ret] = '\0';
        //             char **result = split(buffer, ' ');
        //             strcpy(command, result[0]);
        //             if (strcmp(command, "upload") == 0)
        //             {
        //                 print("uploaaaaaaaaaaaad");
        //             }
        //             else if (strcmp(command, "download") == 0)
        //             {
        //                 print("downloaaaaaaaaaaad");
        //             }
        //             else
        //             {
        //                 print("Command Not Found!\n");
        //             }
        //         }
        //     }
        // }
    }

    return 0;
}
