#include "headers.h"
#include "utils.h"

void *get_in_addr(struct sockaddr *sa);

int broadcast(int sockfd, struct sockaddr_in *broadcastAddr, const char *message);
int receiveUDP(int sockfd, struct sockaddr_in *receiveAddr, char *recievdMessage, int maxReceiveLen);
