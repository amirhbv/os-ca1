#include "headers.h"

void print(const char *message);
void reverse(char s[]);
void itoa(int n, char s[]);
void printInt(int message);
void logError(char *message);
char **split(char *a_str, const char a_delim);
